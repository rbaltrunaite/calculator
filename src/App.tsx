import React, { useReducer } from 'react';
import { Calculator } from './components/Calculator';
import { Requirements } from './components/Requirements';
import './App.scss';

// @ts-ignore
export const AppContext = React.createContext();
const initialState = '';

function reducer(state: string, action: any) {
    if (action.type === 'UPDATE_TITLE') {
        return {
            inputText: action.data
        };
    } else {
        return initialState;
    }
}

const App: React.FC = () => {
    // @ts-ignore
    const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <div className='cont'>
        <AppContext.Provider value={{ state, dispatch }}>
          <Calculator />
          <Requirements />
        </AppContext.Provider>
    </div>
  );
};

export default App;
