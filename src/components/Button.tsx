import React from 'react';

interface CalcButtonProps {
  char: any;
  onClick: () => void
}

export const CalcButton: React.FC<CalcButtonProps> = props => <div className='calc-button' onClick={props.onClick}>{props.char}</div>;
