import React, { useContext } from 'react';
import { AppContext } from '../App'
import { Calculation } from './Calculation';
import '../App.scss';

export const Calculator: React.FC = () => {
  const {state} = useContext(AppContext);

  return (
    <div className='calculator-wrapper'>
      <h2 className='calculator-title'>{state.inputText}</h2>
        <Calculation changedInputText={state.inputText}/>
    </div>
  );
};
