import React from "react";
import { CalcButton } from "./Button";

interface MyProps {
    changedInputText: string
}

interface MyState {
    characters: string[];
    input: string,
    result: number,
    handleClick?: (event: React.MouseEvent<HTMLDivElement>) => void
}

export class Calculation extends React.Component<MyProps, MyState> {
    constructor(props: MyProps) {
        super(props);

        this.state = {
            characters: ['=', 'CE', '*', '/', '-', '+', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '0', 'C'],
            input: '',
            result: 0
        }
    }

    componentDidUpdate(prevProps: { changedInputText: string; }) {
        if (this.props.changedInputText !== prevProps.changedInputText) {
            this.setState({input: ''});
            this.setState({result: 0});
        }
    }

    handleCalculation = (character: any) => {
        let input = this.state.input;
        let result = this.state.result;

        switch (character) {
            case "=":
                if (input !== '') {
                    try {
                        result = eval(this.state.input);
                        this.setState({result: result });
                    } catch (e) {
                        if (e) {
                            this.setState({input: 'Something went wrong!'});
                        }
                    }
                }
                this.setState({result: result });
                document.addEventListener('click', () => {
                    this.setState({result: 0});
                    this.setState({input: ''});
                }, {once : true});
                break;
            case "CE":
                this.setState({input: this.state.input.slice(0, input.length - 1)});
                break;
            case "C":
                this.setState({input: ''});
                this.setState({result: 0});
                break;
            default:
                this.setState({input: this.state.input + character});
        }
    };

render () {
        return (
            <div className='calculator'>
                <div>
                    <h3 className='result-small'>{this.state.input}</h3>
                </div>
                <div className='result'>
                    <h1>{this.state.result}</h1>
                </div>
                {this.state.characters.map(character => {
                    return <CalcButton
                        char={character}
                        key={character}
                        onClick={() => this.handleCalculation(character)}/>
                })}
            </div>
        )
    }
}