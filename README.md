# Calculator

Calculator: a task given by Alna Software

## Table of contents
- [Getting Started](#markdown-header-getting-started)
    * [Technologies](#markdown-header-technologies)
    * [Setup](#markdown-header-setup)
- [Testing](#markdown-header-testing)

## Getting Started

### Technologies

    * React 16.12.0
    * TupeScript 3.4.5
    
### Setup

To run this project, install it locally using npm:

```
npm install 
```

And then run it:

```
npm start
```

